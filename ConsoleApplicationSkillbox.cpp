﻿#include <iostream>


class Vector
{
public:
	Vector()
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}
private:
	double x = 12;
	double y = 42;
	double z = 53;
public:
	void MV()
	{
		double length = sqrt(x * x + y * y + z * z);
		std::cout << '\n' << length;
	}

};


int main()
{
	Vector v;
	v.Show();
	v.MV();
} 